#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initialize( void )
{
    ui->glwidget->initialize();

    ui->group_effects->setId(ui->radio_phong, 0);
    ui->group_effects->setId(ui->radio_orennayar, 1);
    ui->group_effects->setId(ui->radio_toon, 2);
    ui->group_effects->setId(ui->radio_ssao, 3);

    connect(ui->group_effects, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), ui->glwidget, &GLWidget::toggleEffect);
    connect(ui->button_reload_shaders, &QPushButton::clicked, ui->glwidget, &GLWidget::reloadShaders);


    connect(ui->slider_phong_ambient, &QSlider::valueChanged, ui->glwidget, &GLWidget::setPhongAmbient);
    connect(ui->slider_phong_diffuse, &QSlider::valueChanged, ui->glwidget, &GLWidget::setPhongDiffuse);
    connect(ui->slider_phong_specular, &QSlider::valueChanged, ui->glwidget, &GLWidget::setPhongSpecular);
    connect(ui->slider_phong_shininess, &QSlider::valueChanged, ui->glwidget, &GLWidget::setPhongShininess);

    connect(ui->slider_ssao_intensity, &QSlider::valueChanged, ui->glwidget, &GLWidget::setSSAOIntensity);
    connect(ui->slider_ssao_radius, &QSlider::valueChanged, ui->glwidget, &GLWidget::setSSAORadius);
    connect(ui->slider_ssao_blur, &QSlider::valueChanged, ui->glwidget, &GLWidget::setSSAOBlur);
    connect(ui->slider_toon_level, &QSlider::valueChanged, ui->glwidget, &GLWidget::setToonQuantLevel);
    connect(ui->slider_orennayar_roughness, &QSlider::valueChanged, ui->glwidget, &GLWidget::setOrenNayarRoughness);
    connect(ui->check_trackball, &QCheckBox::stateChanged, ui->glwidget, &GLWidget::toggleDrawTrackball);
}

void MainWindow::keyPressEvent(QKeyEvent *ke)
{
    int key = ke->key();
    int modifiers = ke->modifiers();

    if (modifiers == 0 && key == Qt::Key_Escape)
    {
        close();
    }

    ke->accept();
}
