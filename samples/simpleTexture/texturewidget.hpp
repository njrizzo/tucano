#ifndef __TEXTUREWIDGET__
#define __TEXTUREWIDGET__

#include <GL/glew.h>
#include <rendertexture.hpp>
#include <utils/ppmimporter.hpp>

using namespace std;

class TextureWidget 
{
public:

    explicit TextureWidget(void) {}
    ~TextureWidget (void) {}
    
    /**
     * @brief Initializes the shader effect
     * @param w Width
     * @param h Height
     */
    void initialize (int w, int h);

    /**
     * @brief Resize
     * @param w Width
     * @param h Height
     */
    void resize (int w, int h)
    {
        size = Eigen::Vector2i (w, h);
    }


    /**
     * @brief Repaints screen buffer.
     */
    virtual void render (void);

private:

    /// Render image effect (simply renders a texture)
    Effects::RenderTexture rendertexture;

    /// Texture to hold input image
    Texture image_texture;

    /// Dimensions
    Eigen::Vector2i size;

};

#endif // TEXTUREWIDGET
