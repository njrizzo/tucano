#include "simplewidget.hpp"

void ModelWidget::initialize (int width, int height)
{
    // initialize the shader effect (if TUCANOSHADERDIR is set, no need to set dir before init)
    phong.initialize();

    camera.setPerspectiveMatrix(60.0, (float)width/(float)height, 0.1f, 100.0f);
    camera.setRenderFlag(true);
    camera.setViewport(Eigen::Vector2f ((float)width, (float)height));

    light.setRenderFlag(false);
    light.setViewport(Eigen::Vector2f ((float)width, (float)height));

}

void ModelWidget::render (void)
{
    glClearColor(1.0, 1.0, 1.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    phong.render(mesh, camera, light);
    camera.render();
}

void ModelWidget::setShaderDir(std::string dir)
{
    shader_dir = dir;
}

void ModelWidget::openMeshFile(std::string fn)
{
    MeshImporter::loadPlyFile(&mesh, fn);
    mesh.normalizeModelMatrix();
}
