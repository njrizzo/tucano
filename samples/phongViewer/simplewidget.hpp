#ifndef __MODELWIDGET__
#define __MODELWIDGET__

#include <GL/glew.h>
#include <utils/trackball.hpp>
#include <phongshader.hpp>
#include <utils/plyimporter.hpp>

using namespace std;

class ModelWidget 
{

private:

    /// Mesh to be rendered in this widget
    Tucano::Mesh mesh;

    /// Trackball for manipulating the camera
    Tucano::Trackball camera;

    /// Trackball for manipulating light direction
    Tucano::DirectionalTrackball light;

    /**
     * @brief meshFile
     * Create the two private vars to handle
     * a Mesh File and Shaders Dir
     */
    string meshFile;
    string shader_dir;

    /// Phong effect to render mesh
    Effects::Phong phong;

public:

    ModelWidget(void) {}
    ~ModelWidget(void) {}
    
    /**
     * @brief Initializes the widget and shaders
     * @param width Widget width in pixels
     * @param height Widget height in pixels 
     */
    void initialize(int width, int height);

    /**
     * Repaints screen buffer.
     **/
    virtual void render();

    /**
    * @brief Returns a pointer to the camera instance
    * @return pointer to trackball camera
    **/
    Trackball* getCamera (void)
    {
        return &camera;
    }

    /**
    * @brief Returns a pointer to the light instance
    * @return pointer to trackball light
    **/
    DirectionalTrackball* getLight (void)
    {
        return &light;
    }

    /**
     * @brief setMeshFile
     * @param fn
     * New function to setup a meshFile var
     */
    void openMeshFile(string fn);

    /**
     * @brief setShaderDir
     * @param dir
     * New function to setup a shaderVar
     */
    void setShaderDir(string dir);


};

#endif // MODELWIDGET
