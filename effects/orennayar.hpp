/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ORENNAYAR__
#define __ORENNAYAR__

#include <tucano.hpp>
#include <camera.hpp>

using namespace Tucano;

namespace Effects
{

/**
 * @brief Renders a mesh using a Oren-Nayar BRDF shader.
 */
class OrenNayar : public Effect
{

private:

    /// Phong Shader
    Shader orennayar_shader;

	/// Default color
	Eigen::Vector4f default_color;

    /// Roughness coefficient
    float roughness;

public:

    /**
     * @brief Default constructor.
     */
    OrenNayar (void)
    {
		default_color << 0.7, 0.7, 0.7, 1.0;

        roughness = 1.0;

        #ifdef TUCANOSHADERDIR
        setShadersDir(stringify(TUCANOSHADERDIR));
        #endif
    }

    /**
     * @brief Default destructor
     */
    virtual ~OrenNayar (void) {}

    /**
     * @brief Load and initialize shaders
     */
    virtual void initialize (void)
    {
        loadShader(orennayar_shader, "orennayar") ;
    }

	/**
	* @brief Sets the default color, usually used for meshes without color attribute
	*/
	void setDefaultColor ( Eigen::Vector4f& color )
	{
		default_color = color;
	}

    /**
    * @brief Set roughness coefficient
    * @param value New roughness coeff (ro)
    */
    void setRoughnessCoeff (float value)
    {
        roughness = value;
    }

    /** * @brief Render the mesh given a camera and light, using a Oren Nayar brdf shader 
     * @param mesh Given mesh
     * @param camera Given camera 
     * @param lightTrackball Given light camera 
     */
    void render (Tucano::Mesh& mesh, const Tucano::Camera& camera, const Tucano::Camera& light)
    {

        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        orennayar_shader.bind();

        // sets all uniform variables for the Oren-Nayar shader
        orennayar_shader.setUniform("projectionMatrix", camera.getProjectionMatrix());
        orennayar_shader.setUniform("modelMatrix", mesh.getModelMatrix());
        orennayar_shader.setUniform("viewMatrix", camera.getViewMatrix());
        orennayar_shader.setUniform("lightViewMatrix", light.getViewMatrix());
        orennayar_shader.setUniform("has_color", mesh.hasAttribute("in_Color"));
		orennayar_shader.setUniform("default_color", default_color);
        orennayar_shader.setUniform("sigma", roughness);

        mesh.setAttributeLocation(orennayar_shader);

        glEnable(GL_DEPTH_TEST);
        mesh.render();

        orennayar_shader.unbind();
    }

};

}

#endif
