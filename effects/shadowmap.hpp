/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SHADOWMAP__
#define __SHADOWMAP__

#include "tucano.hpp"
#include "utils/trackball.hpp"
#include <math.h>

using namespace std;

using namespace Tucano;

namespace Effects
{

/**
 * Simple Shadow Mapping
 * Renders depth buffer from light position to compute the shadow map
**/
class ShadowMap: public Effect {

protected:
    /// Framebuffer to store the shadow depth map 
    Framebuffer fbo;

    /// Creates the shadowmap depth buffer by rendering from light pos 
    Shader shadowbuffer_shader;

    /// Shader to render shadow map
    Shader drawbuffer_shader;

    /// Quad for rendering shadow map, mostly for debug
    Tucano::Mesh quad;

    int depth_tex_id;

public:

    /**
     * @brief Default constructor.
     *
	**/
    ShadowMap ()
    {
        depth_tex_id = 0;
        quad.createQuad();
	}

    ///Default destructor. 
    ~ShadowMap()
    {
    }

    /**
     * @brief Initializes the ShadowMap effects,
     *
     */
    virtual void initialize (void)
    {
		loadShader(shadowbuffer_shader, "shadowbuffer");
        loadShader(drawbuffer_shader, "rendershadowmap");
    }

	void clearShadowBuffer (void)
	{
		fbo.clearAttachments();
	}


	Framebuffer* getShadowMap (void)
	{
		return &fbo;
	}


    void renderBuffer (const Camera& camera)
    {
        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
        drawbuffer_shader.bind();
        drawbuffer_shader.setUniform("shadowmap", fbo.bindAttachment(0));
        quad.setAttributeLocation(drawbuffer_shader);
        quad.render();
        drawbuffer_shader.unbind();
        fbo.unbindAttachments();
        
    }

    /**
     * @brief Composes the shadow depth buffer
     * @param mesh Mesh to be rendered.
     * @param camera A pointer to the camera trackball object.
     * @param light A pointer to the light trackball object.
     */
    void render (Mesh& mesh, const Camera& camera, const Camera & light)
	{
        glEnable(GL_DEPTH_TEST);
        Eigen::Vector4f viewport = camera.getViewport();
        Eigen::Vector2i viewport_size = camera.getViewportSize();

        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        // check if viewport was modified, if so, regenerate fbo
        if (fbo.getWidth() != viewport_size[0] || fbo.getHeight() != viewport_size[1])
        {
            fbo.create(viewport_size[0], viewport_size[1], 1);
        }

        // Bind buffer to store coord
        fbo.bindRenderBuffer(depth_tex_id);

        shadowbuffer_shader.bind();
        shadowbuffer_shader.setUniform("projectionMatrix", light.getProjectionMatrix());
        shadowbuffer_shader.setUniform("modelMatrix",mesh.getModelMatrix());
        shadowbuffer_shader.setUniform("viewMatrix", light.getViewMatrix().inverse());

        mesh.setAttributeLocation(shadowbuffer_shader);
        mesh.render();

        shadowbuffer_shader.unbind();
        fbo.unbind();

    }

};
}


#endif
