#version 430

uniform sampler2D tex;

out vec4 out_Color;

void main()
{
  vec3 result = texelFetch(tex, ivec2(gl_FragCoord.xy), 0).rgb;
  out_Color = vec4(result, 1.0);
}
