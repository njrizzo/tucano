#version 150

in vec4 color;
in vec3 normal;
in vec4 vert;
in vec4 shadow_vert;

in vec2 texCoords;
in float depth;

out vec4 out_Color;

uniform mat4 lightViewMatrix;
uniform mat4 lightProjectionMatrix;
uniform mat4 modelMatrix;

uniform sampler2D shadowmap;

void main(void)
{
       // if the light did not render it, assume it is not in shadow
//    if (p_shadow.x < 0.0 || p_shadow.x > 1.0 ||p_shadow.y < 0.0 || p_shadow.y > 1.0)
//        shadow_factor = 1.0;

    vec3 lightDirection = (lightViewMatrix * vec4(0.0, 0.0, 1.0, 0.0)).xyz;
    lightDirection = normalize(lightDirection);

    vec3 lightReflection = reflect(-lightDirection, normal);
    vec3 eyeDirection = -normalize(vert.xyz);
    float shininess = 100.0;

    vec4 ambientLight = color * 0.5;
    vec4 diffuseLight = color * 0.4 * max(dot(lightDirection, normal),0.0);
    vec4 specularLight = vec4(1.0) *  max(pow(dot(lightReflection, eyeDirection), shininess),0.0);

	// check shadow map
   	vec4 proj = lightProjectionMatrix * inverse(lightViewMatrix) * modelMatrix * shadow_vert;
   	proj /= proj.w;
   	proj.xy = (proj.xy + vec2(1.0)) * 0.5;

    float cosTheta = dot(lightDirection, normal);
    cosTheta = clamp(cosTheta, 0.0, 1.0);
    float bias = 0.05*tan(acos(cosTheta)); // cosTheta is dot( n,l ), clamped between 0 and 1
     bias = clamp(bias, 0,0.01); 
    bias = 0.5;

  	vec4 shadowbuf = texture (shadowmap, proj.xy);
	float dist = (inverse(lightViewMatrix) * modelMatrix * shadow_vert).z;
       
   	float shadow_factor = 1.0;
	if ((dist - shadowbuf.z) < -bias)
  	    shadow_factor = 0.4;

    out_Color = vec4(ambientLight.xyz + diffuseLight.xyz + specularLight.xyz,1.0) * shadow_factor;
}
